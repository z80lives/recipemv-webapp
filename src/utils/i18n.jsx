import i18n from "i18next";
import { initReactI18next } from "react-i18next";

function initLocale(){
    i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: {
        dv: {
            translation: {
                "welcome": "މަރުހަބާ",
                "Sold Items": "ވިކުނު އަދަދު",
                "Available Items": "ލިބެން ހުރި އަދަދު",
		"Home": "ހޯމް",
		"Profile": "ޕްރޯފައިލް",
		"Recipe": "ރެސިޕީ",
		"Dish": "ޑިޝް",
		"Video": "ވިޑިޔޯ",
		"Contact": "ގުޅުއްވާ"
            }
        }
        },
        lng: "dv",
        fallbackLng: "dv",

        interpolation: {
        escapeValue: false
        }
    });
};

export default initLocale;
