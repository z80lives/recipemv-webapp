import React from 'react';
import "../styles/default.scss";
import logo from "../images/logo.png";

import NavBar from "./main/NavBar";
import Footer from "./Footer";

import {Container, Row, Col} from 'react-bootstrap';

import { useHistory } from "react-router-dom";

import {Button, Modal, Form, FormCheck, Alert} from 'react-bootstrap';

import WebAPI from "../api";
import AuthContext from "../context/auth-context";

//import { throwStatement } from '@babel/types';
//const api = new WebAPI();
/*
class RTLFormCheck extends React.Component{
  render(){
    const props = {...this.props};
    //if(props.ref)
     // delete props["ref"];
      return (
        <Form.Check
                {...props} 
              >
                  <FormCheck.Label  className='pr-5' >{this.props.text}</FormCheck.Label>
                  <FormCheck.Input name={props.name}  isInvalid type={"radio"} />
          </Form.Check>
      );
  }
}*/

class LoginForm extends React.Component{
  //const [show, setShow] = React.useState(false);
  constructor(props){
    super(props);
    this.state = {
      show:false,
      errorMessage: null
    };
    this.api = new WebAPI();
    this.emailRef = React.createRef(); 
    this.passwordRef = React.createRef(); 
    this.props = props;
  }

  static contextType = AuthContext;
  
  handleClose = () => this.setState({show: false});
  handleShow = () => this.setState({show:true});

  handleSubmit(){
    const email = this.emailRef.current.value;
    const password = this.passwordRef.current.value;
    console.log("Submit pressed");
    this.api.auth.login({email: email,
             password: password}, 
             (response)=> {
               if(response.data.login.token){
                 console.log(response.data.login)
                 this.context.login(
                   response.data.login.token, 
                    {
                      user_id: response.data.login.user_id, 
                      user_type: response.data.login.type
                    },
                    response.data.login.token_expiration
                  );
                 this.handleClose();
                 console.log("Login successful", this.context);
                 this.props.redirect();
               }else{
                 this.setState({errorMessage: "Token not received."});                 
               }
             },
             (msg) => this.setState({errorMessage: msg})
             ); 
  };

  onSubmit(e){
    e.preventDefault();
    this.handleSubmit();
  }
  
  render(){
  return (
    <>
      <Button variant="secondary" style={{backgroundColor: "red"}} onClick={this.handleShow}>
        {this.props.children}
      </Button>

      <Modal show={this.state.show} onHide={this.handleClose}>
        <Modal.Header>
          <Modal.Title className={"ml-auto"}> ! ލޮގިން ކުރައްވާ </Modal.Title>
        </Modal.Header>
        <Modal.Body>        
            <Form onSubmit={e => {this.onSubmit(e)}}>
              <Form.Control className={"englishFont"} ref={this.emailRef} type="email" placeholder="އީމެއިލް" />
              <Form.Control className={"englishFont"} ref={this.passwordRef} type="password" placeholder="ޕާސްވޯރޑު" />
                <Button style={{display:"none"}} type="submit" />
            </Form>
            {this.state.errorMessage!=null?
            <Alert key={1} variant={'warning'}>
              {this.state.errorMessage}
            </Alert>:<></>
            }
        </Modal.Body>
        <Modal.Footer className={"mr-auto"}>
          <Button variant="secondary" onClick={this.handleClose}>
            ކެންސަލް
          </Button>
          <Button variant="primary" type="submit" onClick={this.handleSubmit.bind(this)}>
            ލޮގިން
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
  }
}

class RegisterForm extends React.Component{
 //const [show, setShow] = React.useState(false);
 constructor(props){
  super(props);
  this.state = {
    show:false,
    errorMessage: null
  };

  this.nameRef = React.createRef();
  this.emailRef = React.createRef(); 
  this.passwordRef = React.createRef(); 
  this.confirmRef = React.createRef(); 
  
  this.isChef = React.createRef();
  this.isBakery = React.createRef();
  this.isRestaurant = React.createRef();
  this.props = props;
}

static contextType = AuthContext;

handleClose = () => this.setState({show: false});
handleShow = () => this.setState({show:true});

handleSubmit(props){
  const formData = {
    email: this.emailRef.current.value,
    password: this.passwordRef.current.value,
    name: this.nameRef.current.value
  }

  if(this.isRestaurant.current.checked || this.isBakery.current.checked){ 
    const type=this.isRestaurant.current.checked?"restaurant":"bakery";
    formData.type = type;
    if(formData.password === this.confirmRef.current.value){      
      props.redirect(formData);    
      this.handleClose();  
    }else{
      this.setState({errorMessage: "Please make sure your passwords match!"});   
    }
  }

  else if(this.isChef.current.checked){
    console.log("Registering new chef..");
    props.newChefRoute(formData)  
    this.handleClose(); 
  }
}

render(){
  return (
    <>
      <Button variant="secondary" style={{backgroundColor: "red"}} onClick={this.handleShow}>
        {this.props.children}
      </Button>

      <Modal show={this.state.show} onHide={this.handleClose}>
        <Modal.Header >
          <Modal.Title className={"ml-auto"}>ރެޖިސްޓަރ ކުރައްވާ</Modal.Title>
        </Modal.Header>
        <Modal.Body>            
            <Form>
                <Form.Control ref={this.nameRef} type="text" placeholder="ޝެފް، ރެސްޓޯރަނޓު، ބޭކަރީގެ ނަން" />
                <Form.Control ref={this.emailRef} type="email" placeholder="އީމެއިލް" />
                <Form.Control ref={this.passwordRef} type="password" placeholder="ޕާސްވޯރޑު" />
                <Form.Control ref={this.confirmRef} type="password" placeholder="މަތީގައި ޖެއްސެވި ޕާސްވޯރޑު" />

                <Form.Group as={Row}>
                <Col sm={{span:5, offset:3}} >
                <Form.Check
                    defaultChecked
                    ref={this.isChef}
                    label={"ޝެފް"}
                    type={"radio"}
                    name={"accountType"}
                    id={`create-choice-1`}
                  />
                 <Form.Check
                    ref={this.isRestaurant}
                    type={"radio"}
                    name={"accountType"}
                    label={`ރެސްޓޯރަނޓު`}
                    id={`create-choice-2`}
                  />
                <Form.Check
                  ref={this.isBakery}
                  type={"radio"}
                  name={"accountType"}
                  label={`ބޭކަރީ`}
                  id={`create-choice-3`}
                />
                </Col>
                <Form.Label as="legend" column sm={1}>
                  އެކައުންޓް ނަންގަވާ
                </Form.Label>
                </Form.Group>

            </Form>
            {this.state.errorMessage&&
              <Alert key={1} variant={'warning'}>
              {this.state.errorMessage}
             </Alert>
            }
        </Modal.Body>
        <Modal.Footer className={"mr-auto"}>
          <Button variant="secondary" onClick={this.handleClose}>
            ކެންސަލް
          </Button>
          <Button variant="primary" onClick={this.handleSubmit.bind(this, this.props)}>
            ރެޖިސްޓަރ
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
}

const LoginPanel = (props) => {
  let history = useHistory();
  
  return (
      
      <AuthContext.Consumer>              
      { (context) => {
        return (      
        <div className="loginPanel">          
        {context.token == null?
            <><RegisterForm
            redirect={
              (data) => {
                
                history.push({
                  pathname: '/restaurant/new',                  
                  state: { ...data }
                })
              }
            }

            newChefRoute={
              (data) => {
                history.push({
                  pathname: '/chef/new',                  
                  state: { ...data }
                })
              }
            }

            >ރެޖިސްޓަރ</RegisterForm>        
            
            |
            <LoginForm   
            redirect={()=>{history.push("/home");}}       
            >ލޮގިން</LoginForm>   
            </>
        : 
        <Button className={"dhivehiFont"} variant="secondary" style={{backgroundColor: "red"}} onClick={()=>{
          history.push("/home");
          context.logout();
        }}>
           ލޮގް އައުޓު
        </Button>
        }                   
        </div>
        );  
        }}
        </AuthContext.Consumer>
    );
  }

  

const HeaderBox = (props) => (
    <Row className={"headerBox"}>
        <Col ><LoginPanel /></Col>
        <Col >
            <div style ={{textAlign: "right", color:"white", fontsize:"22pt"}} > 
            {props.name}
                <img className="App-logo" src={logo} alt="ރެސިޕީ އެމްވީ" width="140em"/>
            </div>    
        </Col>
    </Row>   
);

const Layout  = (props) => (
  <AuthContext.Consumer>
    {
      (context) => {
        return(
            <Container className="Layout" fluid={"true"}>        
                <HeaderBox  />
                <div className="bodyContainer">          
                    <NavBar isLoggedIn={context.token!=null} layout={"rl"} lang={"dv"}/>
                  {props.children}
                </div>
                <Footer />
            </Container>
        );
      }
    }
  </AuthContext.Consumer>
    
);

export default Layout;
