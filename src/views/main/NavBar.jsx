import React from 'react';
import BootstrapNavbar from 'react-bootstrap/Navbar';
import BootstrapNav from 'react-bootstrap/Nav';
///import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
//import { faCode } from '@fortawesome/free-regular-svg-icons';
import { FaHome } from 'react-icons/fa';
import { FormattedMessage } from 'react-intl';

const nav_routes_dv = [
    {label: "ޕްރޯފައިލް", route: "/ޕްރޯފައިލު"},
//    {label: "ވީޑިޔޯ", route: "/ވީޑިޔޯ"},
    {label: "ސިއްޙަތު", route: "/ސިއްޙަތު"},
//    {label: "ބޭކަރީ", route: "/ބޭކަރީ"},
    {label: "ރެސްޓޯރަނޓު", route: "/ރެސްޓޯރަނޓު"},
    {label: "ޝެފް", route: "/ޝެފް"},
    {label: "ރެސިޕީ", route: "/ރެސިޕީ"},
    {label: "home", route: "/ހޯމް"},
];


const nav_routes_dv_public = [
//    {label: "ވީޑިޔޯ", route: "/ވީޑިޔޯ"},
    {label: "ސިއްޙަތު", route: "/ސިއްޙަތު"},
//    {label: "ބޭކަރީ", route: "/ބޭކަރީ"},
    {label: "ރެސްޓޯރަނޓު", route: "/ރެސްޓޯރަނޓު"},
    {label: "ޝެފް", route: "/ޝެފް"},
    {label: "ރެސިޕީ", route: "/ރެސިޕީ"},
    {label: "home", route: "/ހޯމް"},
];
//const logged_in_navs

//style=
const NavBar = (props) => {
    const data = props.isLoggedIn? nav_routes_dv : nav_routes_dv_public;
    return (
    <BootstrapNavbar variant="default">
        <BootstrapNavbar.Collapse>
            <BootstrapNav className={props.layout==="rl"?"ml-auto mr-auto":"mr-auto"} >                
                {data.map( (el,i) => (
                    <BootstrapNav.Link className={"dhivehiFont homelink"} key={i} href={el.route}>{el.label==="home"?<FaHome/>:el.label}</BootstrapNav.Link>
                ))
                }
            </BootstrapNav>
        </BootstrapNavbar.Collapse>
    </BootstrapNavbar>
   
    );
            }

export default NavBar;
