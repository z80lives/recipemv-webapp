import React from 'react';
import {Row, Col, Image} from 'react-bootstrap';


const Footer = (props) => (      
    <div className="footerBox">
        <Row className="justify-content-md-center">
            <Col md="auto">Newsletter</Col>
        </Row>        
        <Row md="auto" className="justify-content-md-center">
            <Col>
               
                <Row>
                    <Image id="img-logo" src={"./img/logo.png"} style={{width:"10em"}}/>
                </Row>
                <Row>
                    Recipe MV
                </Row>
            </Col>
            <Col>Sitemap</Col>
            <Col>Address</Col>
        </Row>
        <Row className="justify-content-md-center">
            <Col md="auto">&copy; 2019 | Privacy Policy</Col>
        </Row>
    </div>
);

export default Footer;