import React from "react";
import Holder from "holderjs";
import {Button} from "react-bootstrap";
import {toBase64} from "../../helpers";
import { FaUpload } from "react-icons/fa";

export class ProfilePic extends React.Component{
    constructor(props){
        super(props);
        this.input_ref = React.createRef();
    }
    state={
        imgHover: false
    }
    onHover=()=>{
        this.setState({imgHover:true});                
    }
    onHoverOut=()=>{
        this.setState({imgHover:false});    
    }    
    onUploadClick = (e)=>{
        e.preventDefault();
        this.input_ref.current.click()        
    }
    profileChange = async (el)=>{
        console.log("Profile changed", el.target.files);
        const b64 = await toBase64(el.target.files[0]);  
        if(this.props.changePicture){
            this.props.changePicture(b64);
        }      
    }
    render(){        
        return (
            <div
                onMouseEnter={this.onHover}
                onMouseLeave={this.onHoverOut}
                width={"auto"}
                height={"auto"}
              style={{display:"inline-block"}}
	      {...this.props}
            >
                {this.props.children}
                {this.state.imgHover&&
                <Button
                    type="file"
                    onClick={this.onUploadClick}
                    style={{
                        position: "absolute",
                        //top: "30%",
                        //left: "80%"
                        transform: "translate(-470%, 0%)"
                    }}
                > <FaUpload/> </Button>
                }
                <input 
                ref={this.input_ref} type='file' id='single' onChange={this.profileChange} style={{display:"none"}}/>
            </div>
        );
    }
}
