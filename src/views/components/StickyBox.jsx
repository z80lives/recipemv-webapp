import React from 'react';

export default class StickyNav extends React.Component {
    constructor(props){
	super(props);
	const defaultScroll = props.scrollStart || 200;
	this.state = {
	    scrollingLock: false,
	    scrollStart: defaultScroll
	};
	this.handleScroll = this.handleScroll.bind(this);
    }
    
    componentDidMount(){
	window.addEventListener('scroll', this.handleScroll);
    }
    
    componentWillUnmount() {
	window.removeEventListener('scroll', this.handleScroll);
    }    

    handleScroll = () => {
	  if (window.scrollY > this.state.scrollStart) {
	      this.setState({
		  scrollingLock: true
	      });
	  } else if (window.scrollY < this.state.scrollStart) {
	      if(this.props.onScrollDown)
		  this.props.onScrollDown(window.scrollY);
	      this.setState({
		  scrollingLock: false
	      });
	  }
    }

    render(){
	return(
	    <div className={this.state.scrollingLock?"stickyNav":""}>
	      {this.props.children}
	    </div>
	);
    }
}
