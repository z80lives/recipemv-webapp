import React from "react";
import {Card, Button, Row, Col, CardImg} from 'react-bootstrap';

//import ReactStarsRating from 'react-awesome-stars-rating';
import { StarRating } from 'react-star-rating-input';

import { FaSearch, FaHeart, FaCartPlus, FaBookOpen } from 'react-icons/fa';
import {Link} from "react-router-dom";

const styleCircularButton = {
    borderRadius: "50%",
    background: "#000 ",
    background: "rgba(0, 0, 0, 0.5)",
    boxShadow: "0 0 3px gray",
    borderColor: "gray",
    color: "#eee",
    textAlign: "center",
    textDecoration: "none",
    letterSpacing: "1px",
    transition: "all 0.3s ease-out"    
};

const CircleButton = ({children}) => (
    <Button >
        {children}
    </Button>
)

const StyleRecipeThumbContainer = {
    position: "relative",
    width: "50%"
};


export class RecipeGridItem extends React.Component{
state = {
    imgHover:false
}
onHover=()=>{
    this.setState({imgHover:true});        
}
onHoverOut=()=>{
    this.setState({imgHover:false});    
}
openRecipe=()=>{
    //this.props._id;
}
render() {
    const props = this.props;
    return (
    <Card 
        style={{ width: "14em"}}
        onMouseEnter={this.onHover}
        onMouseLeave={this.onHoverOut}   
        className="RecipeThumbCard"         
    >  
        <div className="RecipeThumbContainer" style={StyleRecipeThumbContainer}>      
        <CardImg 
            style={{ padding: "0.5em", width:220, height:180}}
            variant="top" src={props.pictures[0]}></CardImg> 
            {this.state.imgHover &&
            <div className="RecipeThumbBtnGroup">
                <Link to={"/recipe/"+props._id} >
                    <CircleButton><FaSearch/></CircleButton>
                </Link>
                <CircleButton><FaHeart/> </CircleButton> 
                <CircleButton><FaBookOpen/> </CircleButton>         
                <CircleButton><FaCartPlus/></CircleButton>               
            </div>
            }
        </div>    
    <Card.Body>        
    
    <Card.Text align={"center"}>      
    <b>{props.name}</b><br/>
      {props.creator.name}<br/>      
      <StarRating className="ml-auto" value={props.ratings} />
      <br/>
      MVR {props.price}      
      <br/>      
      </Card.Text> 
    </Card.Body>
    <Card.Footer>
      <small className="text-muted">3 mins ago</small>
    </Card.Footer>
    </Card>
);
    }
}

export const RecipeListItem = (props) => (
    <Card style={{margin:"1em"}}>
        {props.mutable && <Card.Header>
            <Button onClick={props.onEdit}>Edit</Button>
            <Button onClick={props.onRemove}>Remove</Button>
        </Card.Header>
        }
        
        <Card.Body>
            <Row>
            <Col sm={6}></Col>
            <Col sm={3}>
            <Card.Title>
                {props.name}                
            </Card.Title>
            <Card.Text>
                
                {props.creator.name}
            </Card.Text>
            </Col>
            <Col sm={3}>
            {props.pictures&& <img height={100} src={props.pictures[0]} alt={"Recipe"} />}  
            </Col>   
            
            </Row>
        </Card.Body> 
    </Card>
);