import React from 'react';
import {Row} from 'react-bootstrap';

const SectionPanel = (props) => (
    <div className="SectionPanel">
        <Row>
            <div className="SectionTitle"><span>{props.title}</span></div>
        </Row>
        {props.children}
    </div>
);

export default SectionPanel;
