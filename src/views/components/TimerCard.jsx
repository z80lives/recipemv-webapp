import React from 'react';
import {FormattedMessage} from 'react-intl';
import Image from 'react-bootstrap/Image';
import {Row, Col, Button, ProgressBar} from 'react-bootstrap';

const TimerCard = (props) => (    
    <Row md={"auto"}>
    <Col> <Image src={props.imgSrc} width={320}/></Col>
    <Col> 
        <Row><Col>{props.title}</Col></Row>
        <Row><Col>{props.subtitle}</Col></Row>
        <Row fluid={"true"}>            
            <Col className={"mr-auto"}> {props.t("Available Items")} :{props.available} </Col>
            <Col className={"mr-auto"}> {props.t("Sold Items")} :{props.sold} </Col>
        </Row>
        <Row> <Col style={{marginBottom:"0.5em"}}><ProgressBar now={30}  /></Col></Row>

        <Row><Col>
        <FormattedMessage 
            id="product.ordernow"
            tagName={Button}
            defaultMessage="Order"            
        />            
        </Col></Row>
    </Col>
    </Row>
);


export default TimerCard;
