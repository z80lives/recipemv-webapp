import React from "react";
import {Image, Button, Form, Alert, Modal, Spinner} from 'react-bootstrap';
import {Redirect} from "react-router-dom";


import AuthContext from "../../context/auth-context";
import PropTypes from "prop-types";
import {ProfilePic} from "../components/ProfilePicture";

export function RegisterTemplate(ChildComponent){
 class RegisterTemplateEx extends React.Component{
    
    static propTypes = {
        location: PropTypes.object.isRequired
      }
    constructor(props){
        super(props);
        //super(props, context);
        const emptyData = {
            address: "Jalan 15",
            name: "Avenue MV",
            email: "avenue@recipe.mv",
            password: "1234",
            description: "Eat good mamak food.",
            island:"Hulhumale'"
        };
        this.img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAAAAACIM/FCAAAAAmJLR0QA/4ePzL8AAAGGSURBVHja7dUxK8RxHMfxK13qJo/BRHeLwaakmCzG0xluxGU5SaFTSmGzGcgiya1SIuUZyMBC6i6OK9/HYPhnuusm6p9e3+EzvLfX8OuXiX9yGRAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBCQ34WsDOTvIm4XK5c/27unFXJ19FEpxGf+rT3STrZ3Ty3kPqKRjfpYxNRJsr17mt/I02isz0SUaske9+/Up587e/ohe2exVI4oLyQbm5MbzW497ZDXakRtNqK4mmx8Du927SmHfK29R5xPRIyfJhuNucFWt55yyPZjvNy0h1rtQivZqDVL1W493ZCtbC7X9xAX88vXkex+MQ6yh53dzw4CAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLyR/cNuCDnSxklpXgAAAAASUVORK5CYII=";
        this.state = {
            currentMessage: "އިންތިޒާރު ކުރައްވާ",
            ...emptyData,
            thumbnail: this.img,
            redirect: false,
            errorMessage: null,
            showLoading: false,
            registered: false
        };

        this.form = {
            name: React.createRef(),
            address: React.createRef(),
            email: React.createRef(),
            password: React.createRef(),
            description: React.createRef(),
            agree: React.createRef(),
            island: React.createRef()           
        }        
    }

    
    componentDidMount(){
        //console.log("obj",this.context.api.restaurantSignup);
        const {location} = this.props
        if(location.state){
            this.fillForm(location.state);
        }
    }

    
    fillForm(data){
        //let tb = this.form.name;        
        for(var key in data){
            if (key==="type") //ignore "type"
                continue;
            let element = this.form[key].current;
            element.value = data[key];
        }
    }

    getFormData = ()=> {
        const data={};
        for(var key in this.form){
            data[key] = this.form[key].current.value;
        }
        //return data;
        return {
            name: data.name,
            email: data.email,
            address: data.address,
            password: data.password,
            description: data.description,
            island: data.island,
            thumbnail: this.state.thumbnail,
            loggingIn: false,
            loading: false
        };
    }

    handleClose = () => this.setState({showLoading: false});
    handleShow = () => this.setState({showLoading:true});

    clickLogin=()=>{
        let formData = this.getFormData();
        //this.setState({redirect: true});
        this.setState({currentMessage: "ލޮގިން ކުރަނީ", loading: true});
        this.context.api.auth.login({
            email: formData.email,
            password: formData.password
        },
            (response) => {                

                if(response.data.login.token){
                    this.context.login(response.data.login.token, 
                        {
                            user_id: response.data.login.user_id,
                            user_type: response.data.login.type
                        }, 
                        response.data.login.token_expiration);
                    this.setState({currentMessage: "ލޮގިން ވެއްޖެ", redirect: true, loading: false});                                        
                  }else{
                    this.setState({currentMessage: "ލޮގިން ނުވި، ޓޯކަން ނުލިބުނު. ކަސްޓަމަރ ސަރވިސްއާ ގުޅުއްވާ", loading: false});                                
                  }
            },
            (response) => {
                this.setState({currentMessage: "ލޮގިން ނުވި", loading: false});
            }
        )
    }

    clickBack=()=>{
        this.setState({redirect: true});
    }

    render(){
        const { redirect } = this.state;
        const thumbnail = this.state.thumbnail || "holder.js/200x200";
        if (redirect) {
            return <Redirect to='/'/>;
        }else
        return (
            <>
                <ChildComponent 
                    form={this.form}
                    thumbnail={thumbnail}         
                    errorMessage={this.state.errorMessage} 
                    thumbnailComponent={()=>(
                        <ProfilePic
                            changePicture={
                                (pic) => {this.setState({thumbnail: pic})}
                            }
                        >                        
                            <Image src={this.state.thumbnail} width={200}  />
                        </ProfilePic>
                    )}
                    onSubmit={(submitCallback) => {
                        let formData = this.getFormData();
                        formData.description = formData.description.split("\n").reduce((a,b) => a+"<br/>"+b);
                        if(formData.thumbnail == null){
                            this.setState({errorMessage: "ޕްރޯފައިލް ފޮޓޯއެއް އަޕްލޯޑު ކުރައްވާ"})
                            return false;
                        }
                        if(!this.form.agree.current.checked){            
                            this.setState({errorMessage: "އެއްބަސްވުން ޤަބޫލުނުކޮށް މިވެބްސައިޓު ބޭނުމެއް ނުކުރެވޭނެއެވެ"})
                            return false;
                        }
                        submitCallback(formData, this);
                    }}          
                />
                <Modal show={this.state.showLoading} onHide={this.handleClose}>
                    <Modal.Header>
                        <Modal.Title className={"ml-auto"}>ރެޖިސްޓަރ ކުރަނީ</Modal.Title>
                    </Modal.Header>

                    <Modal.Body className={"ml-auto"}>
                        {this.state.loading &&
                        <div>
                            <Spinner animation="grow" /><br/>
                        </div>
                        }
                        {this.state.currentMessage}
                    </Modal.Body>

                    <Modal.Footer>

                    {this.state.registered &&
                    <>
                    <Button variant="secondary" onClick={this.clickBack}>
                        ފަހަތައް
                    </Button>
                    <Button variant="primary" onClick={this.clickLogin}>
                        ލޮގިން
                    </Button>
                    </>
                    }
                    </Modal.Footer>
                </Modal>
            </>
        );
    }

    static contextType = AuthContext;
}
return RegisterTemplateEx;
}