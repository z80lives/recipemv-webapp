import React from "react";
import {Row, Col, Image, ButtonToolbar, ButtonGroup, Button} from "react-bootstrap";
import Holder from "holderjs";
import "../../styles/ChefHomePage.scss";
import StickyNav from "../components/StickyBox.jsx";

import ChefPage from "./ChefPages/HomePage";
import ProfilePage from "./ChefPages/Profile";
import RecipePage from "./ChefPages/Recipe";
import DishPage from "./ChefPages/Dish";
import VideoPage from "./ChefPages/Video";
import ContactPage from "./ChefPages/Contact";

import {UploadablePic} from "../components/UploadablePic";

import { FaHome, FaUser, FaUtensils, FaBreadSlice, FaFilm, FaIdCard } from 'react-icons/fa';

const floatContainerStyle = {
			   position: "absolute",
			   backgroundColor: "gray",
			   left: 0,
			   top: 0,
			   backgroundImage: `url("holder.js/300x200")`,
			   backgroundSize: "100% 110%",
			   backgroundRepeat: "no-repeat"
};

const coverImageStyle = {
    ...floatContainerStyle,
    width: "100%",
    height: "100%",
    overflow: "hidden"
};

const profileBoxStyle = {...floatContainerStyle,
			 width: "12em",
			 minHeight: "15em",
			 margin: "0.5em",
			 padding: "1em",
			 backgroundColor: "#20a2e8",
			 left: "70%",
			 color:"white",
			 overflowWrap: "break-word"
			};

const profileImageStyle = {
    width: "5em",
    height: "4em",
    left: "12%",
    margin: "1em",
    backgroundColor: "gray",
    position: "relative",
    borderRadius: "25px",
    padding: "3em",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    backgroundRepeat: "no-repeat"
};

const ProfileBox = (props) =>
      (
	  <div style={profileBoxStyle} >
	    <div style={{...profileImageStyle, backgroundImage: `url(${props.thumbnail})`}}></div>
	    <div>{props.name}</div>
	    <div>{props.type.reduce((a,b) => a + "," + b )}</div>
	    <div>{props.restaurants}</div>
            <div><a style={{color:"red"}} href={props.url}>{props.url}</a></div>
	  </div>
      );

const chefNavBoxStyle = {
    display: "inline",    
};

const ChefNavBox = (props) => {
    /*const buttons = [
	"Home", "Profile", "Recipe", "Dish", "Video", "Contact"
    ];
    const Icon = [<FaHome/>, <FaUser/>, <FaUtensils/>, <FaBreadSlice/>, <FaFilm/>, <FaIdCard/>
		 ].reverse();
    */
    const buttons = [
	"Home", "Profile", "Recipe", "Contact"
    ];
    const Icon = [<FaHome/>, <FaUser/>, <FaUtensils/>, <FaIdCard/>
		 ].reverse();
    return (
	<StickyNav scrollStart={300}>
	  <ButtonToolbar>
	    <ButtonGroup>
	      {buttons.reverse().map((el,idx) =>
				     <Button
					   variant="danger"
					   key={"profile-btn-"+idx}
					   onClick={()=>props.onChange(el)}
					   active={el===props.currentPage?true:false} >
				     <div>
					   { Icon[idx] }
					       <p>{props.t(el)}</p>
					 </div>
			       </Button>
			  )
	      }
	    </ButtonGroup>
	  </ButtonToolbar>
	</StickyNav>
    
    );
}


const PageViewChanger=(props)=>{
    if (props.pageId in props.pageMap){
	
	const ViewFrag = props.pageMap[props.pageId];
	return(
	    <ViewFrag t={props.t} data={props.data} isOwner={props.isOwner}/>
	);
    }else{
	return (<div>Page not mapped </div>);
    }
};


export default class ChefHomePage extends React.Component{
    state={
	currentPage: "Home",
	pageMap: {
	    "Home": ChefPage,
	    "Profile": ProfilePage,
	    "Recipe": RecipePage,
	    "Dish": DishPage,
	    "Video": VideoPage,
	    "Contact": ContactPage
	},
	data: {
	    chefData: null,
	    coverPicture: null
	},
	//isOwner: this.props.isOwner
    }

    componentWillMount(){
	const ctx = this.props.context;
	//console.log("ctx", ctx.user_id);
	
	ctx.api.chef.getChefData(this.props.chefId, (data)=>{
	    this.setState({data: {...this.state.data,
				  chefData: data				 
				 },
			   coverPicture: data.coverImage
			  });
	}, this.props.isOwner);
    }

    constructor(props){
	super(props);
    }

    changeCoverPic = (img) => {
	const ctx = this.props.context;

	console.log("Changing cover pic");
	ctx.api.chef.updateCoverPic(this.state.data.chefData._id,
				    img,
				    (success) => {
					const img = success.coverImage;
					this.setState({coverPicture: img});
				    }
				   )	
    }

    render(){
	const chefData = this.state.data.chefData;
	//var coverPicture = "https://cfvod.kaltura.com/p/1727411/sp/172741100/thumbnail/entry_id/1_yvf9316q/version/100000/width/412/height/248";
	const coverPicture = this.state.coverPicture? this.state.coverPicture: "https://cfvod.kaltura.com/p/1727411/sp/172741100/thumbnail/entry_id/1_yvf9316q/version/100000/width/412/height/248";
	//if(chefData.coverImage)
	 //   coverPicture = chefData.coverImage;
        return(
            <>
              <Row>		
		<div style={{width: "100%", height: "15em", position: "relative"}}>

                  <div style={coverImageStyle} >
		    {this.props.isOwner &&
			<UploadablePic style={{width:"100%", height:"100%"}}
					   changePicture={this.changeCoverPic}
					   >
			      <img style={{width:"100%", height:"100%"}} src={coverPicture} /> 
			</UploadablePic>
			}
			{!this.props.isOwner&&
			    <img style={{width:"100%", height:"100%"}} src={coverPicture} /> 
			}
		  </div>

		  
		  {this.state.data.chefData &&
		  <ProfileBox
			name={chefData.account.name}
			thumbnail={chefData.thumbnail}
			type={["ކަލިނަރީ ލެޖެނޑު", "ހޯމް ޝެފް"]}
			restaurants={["ޕެޓްރެސް"]}
			url={"recipe.mv/gordonramsay"}
			/>
		  }
		</div>
		
            </Row>
	      <Row className="justify-content-md-center">
		<ChefNavBox
		  currentPage={this.state.currentPage}
		  onChange={pageId => this.setState({currentPage: pageId}) }
		  t={this.props.t}
		  />
	      </Row>
	      <PageViewChanger		
		pageId={this.state.currentPage}
		pageMap={this.state.pageMap}
		t={this.props.t}
		data={this.state.data}
		isOwner={this.props.isOwner}
		/>
            </>
        )
    }
}
