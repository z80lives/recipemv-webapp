import React from "react";
import {Button, Image, Container, Col, Row, Card, CardImg} from 'react-bootstrap';
import AuthContext from "../../context/auth-context";

import { StarRating } from 'react-star-rating-input';

import ChefHomePage from "./ChefHomePage";

export const ChefGridItem = (props) => (
    <Card style={{ width: '14em', marginBottom: "3em" }} >        
        <Card.Body>  
            {props.thumbnail && <CardImg src={props.thumbnail}></CardImg>}            
            <Card.Text align={"center"}>      
            <div>
                <b style={{flexDirection: "left"}}>{props.account.name}</b>             
                <StarRating className="ml-auto" value={props.ratings} />
            </div>
            <div>
            {props.contact.island}

            </div>
            </Card.Text> 
        </Card.Body>
    </Card>
);


const ClickableItem = (props) => (
    <div
      className={"clickableItem"}
      {...props}>
      {props.children}
    </div>
)


export class ChefDirectoryView extends React.Component{
    static contextType = AuthContext;
    state={
        data: [],
	renderHome: null,
	selectedChef: null
    }
    componentDidMount(){
        this.context.api.chef.listChef(response => this.setState({data: response.data.chefs}));
    } 

    render(){
	if(this.state.selectedChef){
	    return(
		<>
		  <Button onClick={()=>{this.setState({selectedChef: null})}}>Back</Button>
		<ChefHomePage
		  t={this.props.t}
		  context={this.context}
		  chefId={this.state.selectedChef}
		  isOwner={false}
		  />
		</>
	    );
	}else{
        return (
            <>	      
            {
            this.state.data.length === 0?
            <div>No chef found</div>
            :
            <Container>
                <Row>
                      {
                    this.state.data.map(chef=> {
			//return (<RecipeListItem {...recipe} />);
			return(<Col sm={6} lg={3} >
			       <ClickableItem
			       onClick={
				   ()=>{
				       this.setState({selectedChef: chef._id});
				   }
			       }
				       >
			       <ChefGridItem
			       {...chef}
			       />
			       </ClickableItem>
			       </Col>);
                    })
                }    
                </Row>    
            </Container> 
            }
            </>
        )
	};
    }
}
  

export default ChefDirectoryView;
