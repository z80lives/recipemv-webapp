import Home from "./Home";
import RegisterRestaurant from "./RegisterRestaurant";
import RegisterChef from "./RegisterChef";
import {RecipeDirectory} from "./Recipe";
import {MyRecipe} from "./MyRecipe";
import Video from "./Video";
import Profile from "./Profile";
import Health from "./Health";
import Bakery from "./Bakery";
import Chef from "./Chef";
import RecipeView from "./RecipeView";

export  {
    Home, 
    RegisterRestaurant,
    RegisterChef,
    RecipeDirectory,
    MyRecipe,
    Video,
    Profile,
    Health,
    Bakery,
    Chef,
    RecipeView
};