import React from "react";
import Image from 'react-bootstrap/Image';
import {Row, Col, Button, ProgressBar, Alert} from 'react-bootstrap';
import AuthContext from "../../context/auth-context";
import {FormattedMessage} from 'react-intl';
import TimerCard from "../components/TimerCard";
import SectionPanel from "../components/SectionPanel";

import ChefHomePage from "./ChefHomePage";


class HomePage extends React.Component{
    state = {
        user_name: ""
    };
    static contextType = AuthContext;


render(){
    return (
        <AuthContext.Consumer>
            {(context) =>{
                return (
                    context.user_type && context.user_type === "chef"?
			<ChefHomePage
			      t={this.props.t}
			      context={context}
			      chefId={context.user_id}
			      isOwner={true}
			      />
			    :
			    
			<div>    			    
			      <Row>
				    <Image src={"./img/ad1.png"} fluid={"true"} />
				  </Row>
				  
				  <SectionPanel title={"އަދުގެ ހާއްސަ"}>
					<TimerCard 
					      title={"ވެނީލާ ކަޕްކޭކް ވިތު ރެޑު ޝުގަރ ފްލާރވާސް"}
					    subtitle={"ހަމަ އެނމެ -/15ރ އަށް"}
					      imgSrc={"./img/cupcakes.png"}
					      available={0}
					      sold={0}
					      t={this.props.t}
					      />                                     
				    </SectionPanel>
                        
                        <Row>
                          <Image src={"./img/ad2.png"} fluid={"true"} />
                        </Row>
			
                        <Row>
                          <Image src={"./img/ad3.png"} fluid={"true"} />
                            </Row>
			
                        <Row>
                          <Image src={"./img/ad4.png"} fluid={"true"} />
                        </Row>
                    </div>
                    
                );
            }}
            </AuthContext.Consumer>
    );
    }
}
export default HomePage;  
