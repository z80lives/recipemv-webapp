import React from "react";
import {Button, CardDeck, Card, Container, Col, Row} from 'react-bootstrap';
import {  useParams
} from "react-router-dom";
import AuthContext from "../../context/auth-context";
import { StarRatingInput } from 'react-star-rating-input';

const dummyComments = [
    {id: 0, author: "Ali Thakuru", msg: "Tastes good!", timestamp: 1577414875248, replyTo: null},
    {id: 1, author: "Ali Rasgefaanu", msg: "Thank you.", timestamp: 1577414875248, replyTo: 0}
];

class RecipeView extends React.Component{    
    static contextType = AuthContext;
    state={
        data: null,
        rating: 0
    };

    componentDidMount(){
        this.context.api.recipe.fetchRecipe( (response)=> {
            this.setState({data: response.data.recipes[0]});
            console.log("response", response);
        }, this.props.id);        
    }

    clickRating= (val) => {
        this.setState({rating: val});
    }

    render(){
        const data = this.state.data;
        return (
            <div>
                {data &&
                <Card>
                    <Card.Header>
                    <Card.Title>{data.name}</Card.Title>
                    </Card.Header>
                <Card.Body>                    

                    {data.pictures.map(src => 
                        <div><img src={src} width={320} height={200} /></div>
                    )}
                    <div>
                    {data.instruction}
                    </div>
                    {data.price}
                    {!this.context.token &&
                     <div>Login to rate</div>
                    }
                    <div>Servings: {data.servings}</div>
                    {this.context.token &&
                        <div style={{float:"right"}}>
                        <StarRatingInput name="rating" value={this.state.rating} onChange={this.clickRating} />
                        </div>
                    }   
                </Card.Body>
                </Card>
                }
            </div>
        );
    }
}

const RecipeViewRouteComp = function(){
    let {id} = useParams();
        return <RecipeView id={id}/>
}

export default RecipeViewRouteComp;