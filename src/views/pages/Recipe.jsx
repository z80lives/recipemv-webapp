import React from "react";
import {Button, CardDeck, Container, Col, Row} from 'react-bootstrap';
import AuthContext from "../../context/auth-context";

import {RecipeListItem, RecipeGridItem} from "../components/Recipe";
//import insertCss from 'insert-css';
//import {css} from "react-star-rating-input";

import { FaPlus } from "react-icons/fa";

function RecipeDirectoryView(props){    
    return (
        props.data.length == 0?
        <div>No recipe found</div>
        :
        <Container>
            <Row>
            {
            props.data.map(recipe => {
                //return (<RecipeListItem {...recipe} />);
                return(<Col sm={6} lg={3} ><RecipeGridItem {...recipe}/></Col>);
            })
            }    
            </Row>    
        </Container>
    );
};

export class RecipeDirectory extends React.Component{
    static contextType = AuthContext;
    state={
        data: []
    }

    componentDidMount(){
        const recipeCached = localStorage.getItem('recipeCache') || null;
        
        if(recipeCached)
          this.setState({data: JSON.parse(recipeCached)});

        this.context.api.recipe.listRecipe(response => {
            localStorage.setItem("recipeCache", JSON.stringify(response.data.recipes));
            this.setState({data: response.data.recipes});
        });
        //insertCss(css);
        //console.log(css);
    } 

    clickMyRecipe = ()=>{
        window.location.href='/create/recipe';
    }


    render(){
        return (
            <>
            {this.context.token && 
              <Button onClick={this.clickMyRecipe} className="fixedCircleButton" ><FaPlus/> </Button>
            }
            <RecipeDirectoryView
                data={this.state.data}
            />  
            </>
        )
    }
}
  

//export default RecipeDirectory;