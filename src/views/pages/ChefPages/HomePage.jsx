import React from 'react';
import {Row, Button} from 'react-bootstrap';

import TimerCard from "../../components/TimerCard";
import SectionPanel from "../../components/SectionPanel";


const itemStyle = {width:"40em", height: "10em"};
export default class ChefPage extends React.Component{
    state={
	featuredDishes: [],
	specialDishes: [],
	otherServices: [],
	isAdmin: false
    }    
    render(){
	const isAdmin = this.props.isOwner;
	console.log("isadmin", isAdmin);
	return (
	    <>
	      <Row className="justify-content-md-center">
		<SectionPanel title={"އަދުގެ ހާއްސަ"}>
		<TimerCard 
		  title={"ވެނީލާ ކަޕްކޭކް ވިތު ރެޑު ޝުގަރ ފްލާރވާސް"}
		  subtitle={"ހަމަ އެނމެ -/15ރ އަށް"}
		  imgSrc={"./img/cupcakes.png"}
		  available={0}
		  sold={0}
		  t={this.props.t}
		  />
		 {isAdmin &&
		      <Button>Add</Button>
		      }
		</SectionPanel>
	      </Row>
	      <Row className="justify-content-md-center">
		<SectionPanel title={"ކެއުންް"}>
		  <div style={itemStyle}>
		    No Featured items
		  </div>
		  {isAdmin &&
		      <Button>Add</Button>
		      }
		</SectionPanel>
	      </Row>

	      <Row className="justify-content-md-center">
		<SectionPanel title={"އަދުގެ ޙާއްސަ"}>
		  <div style={itemStyle}>
		    Today s special not available
		  </div>
		  {isAdmin&&
		  <Button>Add</Button>}
		</SectionPanel>
	      </Row>
	      <Row className="justify-content-md-center">
		<SectionPanel title={"އިތުރު ޙިދްމަތްތައް"}>
		  <div style={itemStyle}>
		    No information
		  </div>
		  {isAdmin && 
		  <Button>Add</Button>}
		  
		</SectionPanel>
	      </Row>
	    </>
	);
    }
}

