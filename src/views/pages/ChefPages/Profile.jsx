import React from 'react';
import {Row, Col} from 'react-bootstrap';


const NameCard = (props) => (
    <div>
      <h3> {props.name}</h3>
      <h5> {props.title} </h5>
      <hr/>
    </div>    
);

export default class ProfilePage extends React.Component{
    render(){
	const data = this.props.data.chefData;
	console.log("data", data);
	if(!data)
	    return(<div>Loading</div>);
	return(
	    <>	      
	      <Row>
		<Col md="8" style={{minHeight:"5em"}}>

		</Col>
		<Col md="4" style={{minHeight:"5em"}}>
		  <NameCard
		    name={data.account.name}
		    title={"professional chef"}
		    facebookLikes={100}
		    instaLikes={200}
		    twitterLikes={200}
		    />
		</Col>
	      </Row>
	    </>
	);
    }
}
