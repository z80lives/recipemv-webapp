import React from "react";

import {Image, Button, Form, Alert, Modal, Spinner} from 'react-bootstrap';
import {withRouter} from "react-router-dom";
//import { longStackSupport } from "q";

import {RegisterTemplate} from "../components/RegisterFormComponent";

class RegisterRestaurant extends React.Component{
    constructor(props){
        super(props);
        this.form = props.form;        
    }

    clickSubmit = (formData, parent) =>{
        //console.log("Hei")
        parent.setState({currentMessage: "ރެޖިސްޓަރ ކުރަނީ", loading: true});
        parent.handleShow();
        parent.context.api.auth.restaurantSignup(formData)
            .then(response => {
                let errorMessage = parent.context.api.fetchGraphqlError(response);
                if(errorMessage){
                    throw new Error(errorMessage);                    
                }
                parent.setState({
                    currentMessage: "ރެޖިސްޓަރ ކޮށް ނިމިއްޖެ. ކުރިއައް ގެނދަންވި ގޮތް ނަނގަވާ", 
                    loading:false,
                    registered: true
                });
                //this.setState({registered: true});
                //this.setState({redirect: true});                
            }).catch( error => {
                console.log("error", error.toString());
                parent.setState({errorMessage: error.toString(), loading:"false", registered: false});
                parent.handleClose();
            });  
    }

    render(){
        return (
            <Form>
                <Form.Group  controlId="restaurantName">
                    <Form.Label> ރެސްޓޯރަނޓުގެ ނަން</Form.Label>
                    <Form.Control ref={this.form.name} type="name" placeholder="ރެސްޓޯރަނޓުގެ ނަން ލިޔުއްވާ" />
                    <Form.Text  className="text-muted">
                    އެހެނިހެން މެނބަރުންނާއި މި ސައިޓުގައި މުޙާތަބުކޮށް ހަދާނީވެސް މި ނަމުގައެވެ      
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="profilePicture">
                    <Form.Label>ޕްރޯފައިލް ފޮޓޯ</Form.Label>
                    <div>
                        {this.props.thumbnailComponent()}
                    </div>
                </Form.Group>

                <Form.Group controlId="restaurantAddress">
                    <Form.Label>އެޑްރެސް</Form.Label>
                    <Form.Control ref={this.form.address} type="address" placeholder="އެޑްރެސް ލިޔުއްވާ" />
                </Form.Group>

                <Form.Group controlId="restaurantIsland">
                    <Form.Label>ރަށް</Form.Label>
                    <Form.Control ref={this.form.island} type="address" placeholder="ރަށުގެ ނަން" />
                </Form.Group>

                <Form.Group controlId="restaurantDescription">
                    <Form.Label>މައުލޫމާތު</Form.Label>
                    <Form.Control ref={this.form.description} as="textarea" placeholder="ރެސްޓޯރަނޓު އާއި ބެހޭ މައޫލޫމާތު ބަޔާން ކުރައްވާ" />
                </Form.Group>

                <Form.Group controlId="contactEmail">
                    <Form.Label>އީމޭއިލް</Form.Label>
                    <Form.Control ref={this.form.email} type="email" placeholder="އީމެއިލް އެޑްރެސް ލިޔުވާ" />
                    <Form.Text className="text-muted">
                އަޅުގަނޑުމެން މި އީމެއިލް އެޑްރެސް އެއްވެސް އިތުރު ފަރާތަކާ ހިއްސާއެއް ނުކރުރާނަމެވެ
                    </Form.Text>
                </Form.Group>

                <Form.Group controlId="contactPassword">
                    <Form.Label>ޕާސްވޯރޑު</Form.Label>
                    <Form.Control ref={this.form.password} type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group controlId="agreeCheckBox">                
                    <Form.Check ref={this.form.agree} type="checkbox" label="މި ވެބްސައިޓުގެ ޙިދުމަތުގެ އެއްބަސްވުން ޤަބޫލު ކުރަމެވެ" />                                                
                </Form.Group>
                {this.props.errorMessage&&
                <Alert key={1} variant={'warning'}>
                {this.props.errorMessage}
                </Alert>}
                <Button variant="primary" type="button" onClick={()=>this.props.onSubmit(this.clickSubmit)}>
                    ރެޖިސްޓަރ
                </Button>
            </Form>
         );
    }
}

export default withRouter(RegisterTemplate(RegisterRestaurant));