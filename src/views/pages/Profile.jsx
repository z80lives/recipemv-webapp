import React from "react";
import AuthContext from "../../context/auth-context";

import {Spinner, Image, Button} from "react-bootstrap";
import Holder from "holderjs";

import {ProfilePic} from "../components/ProfilePicture";

export class Profile extends React.Component{
    static contextType=AuthContext;

    state = {
        id: null,
        type:null,    
        data: null,
        newData: {}
    }

    async fetchUserData(){
        const user_data= await this.context.api.auth.getUserData();
	
	if(user_data.type=="restaurant"){
            this.setState({type: user_data.type, id: user_data.restaurant._id});
            const restaurant_data = await this.context.api.restaurant.getRestaurantData(user_data.restaurant._id);        
            this.setState({
		data: {...restaurant_data, profile_pic: restaurant_data.thumbnail}            
            });
	}else if(user_data.type == "chef"){
	    this.setState({type: user_data.type, id: user_data.chef._id});
	    const chef_data = await this.context.api.chef.getChefData(user_data.chef._id);
	    //console.log("chef_data", chef_data);
            this.setState({
		data: {...chef_data, profile_pic: chef_data.thumbnail}            
            });

	}
    }

    componentDidMount(){
        this.fetchUserData();
    }

    saveData = ()=>{
        console.log(this.state.newData);        
        const success = (data) => {
            console.log("success", data)
        };

        if(this.state.type=="restaurant")
            this.context.api.restaurant.updateProfilePic(
                this.state.id,
                this.state.newData.thumbnail,
                success
            );

	if(this.state.type == "chef"){
	    this.context.api.chef.updateProfilePic(
                this.state.id,
                this.state.newData.thumbnail,
                success
            );
	}
    }
    
    render(){
        const variant = this.state.type==null?"primary":"secondary";
        const modified = Object.keys(this.state.newData).length != 0;
        return(            
            <div>
                {this.state.data==null && 
                <Spinner animation="border" variant={variant} role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>                
                }
                <ProfilePic
                    changePicture={(picture)=>{this.setState({
                        data: {...this.state.data, profile_pic: picture},
                        newData: {...this.state.newData, thumbnail: picture}
                    })}}
                >
                {this.state.data && this.state.data.profile_pic? 
                    <Image src={this.state.data.profile_pic} width={200} />
                    :
                    <Image src="holder.js/200x200" width={200}  />
                }
                </ProfilePic>
                {this.state.data &&
                <div>
                 <div> <b className="dhivehiFont">  ނަން </b> : {this.state.data.account.name}</div>
                 <div> <span style={{fontFamily: "Times New Roman, Times, serif"}}>{this.state.data.account.email}</span>  <b className="dhivehiFont"> : އީމެއިލް </b></div>
                </div>                        
                }             
                {modified &&
                <Button 
                    className={"dhivehiFont"}
                    onClick={this.saveData}
                > ސޭވް ކުރައްވާ </Button>
                }   
            </div>
        );
    }
}

export default Profile;
