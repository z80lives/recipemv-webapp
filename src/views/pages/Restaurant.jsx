import React from "react";
import {Button, Image, Container, Col, Row, Card, CardImg} from 'react-bootstrap';
import AuthContext from "../../context/auth-context";

import Holder from "holderjs";

import { StarRating } from 'react-star-rating-input';

const RestaurantGridItem = (props) => (
    <Card style={{ width: '14em', marginBottom: "3em" }} >        
        <Card.Body>  
            {props.thumbnail && <CardImg src={props.thumbnail}></CardImg>}            
            <Card.Text align={"center"}>      
            <div>
                <b style={{flexDirection: "left"}}>{props.account.name}</b>             
                <StarRating className="ml-auto" value={props.ratings} />
            </div>
            <div>
            {props.contact.island}

            </div>
            </Card.Text> 
        </Card.Body>
    </Card>
);

function RestaurantDirectoryView(props){    
    return (
        props.data.length == 0?
        <div>No restaurant found</div>
        :
        <Container>
            <Row>
            {
            props.data.map(restaurant=> {
                //return (<RecipeListItem {...recipe} />);
                return(<Col sm={6} lg={3} ><RestaurantGridItem {...restaurant}/></Col>);
            })
            }    
            </Row>    
        </Container>
    );
};

export class RestaurantDirectory extends React.Component{
    static contextType = AuthContext;
    state={
        data: []
    }
    componentDidMount(){
        this.context.api.restaurant.listRestaurants(response => this.setState({data: response.data.restaurants}));
        //insertCss(css);
        //console.log(css);
    }    
    clickMyRecipe = ()=>{
        window.location.href='/create/recipe';
    }
    render(){
        return (
            <>
            
            <RestaurantDirectoryView
                data={this.state.data}
            />  
            </>
        )
    }
}
  

//export default RecipeDirectory;
