import React from "react";
import {Button, Form, Card, Tabs, Tab} from 'react-bootstrap';
import ImageUploader from 'react-images-upload';
import {toBase64} from "../../helpers";
import AuthContext from "../../context/auth-context";
import {RecipeListItem} from "../components/Recipe";

class CreateRecipe extends React.Component{    
    constructor(props){
        super(props);
        this.form = {
            name: React.createRef(),
            instruction: React.createRef()
        }
        this.state = {
            pictures: []
        }
    }


    static contextType = AuthContext;

    onSubmit = async () => {
        const data={};
        for(var key in this.form){
            data[key] = this.form[key].current.value;
        }
        console.log(this.form);
        console.log(data);
        console.log(this.state.pictures[0])
        data["pictures"] = [];
        
        for(let i = 0; i < this.state.pictures.length; i++){
            const b64img = await toBase64(this.state.pictures[i]);
            data["pictures"].push(b64img);
        }
        
        this.context.api.recipe.createRecipe(data, (result)=> {
            console.log("Success", result);
            if(this.props.submitCallback)
                this.props.submitCallback(result);
        });
    }

    onDrop = (pictureFiles, pictureDataURLs)=>{
        console.log(pictureFiles);
        this.setState({
            pictures: this.state.pictures.concat(pictureFiles)
        });
    }

    render(){
        return (
            <Form>                
                <Form.Group controlId="recipeType">
                    <Form.Label>Recipe Type   </Form.Label>                    
                </Form.Group>

                <Form.Group  controlId="recipeName">
                    <Form.Label>Recipe Name</Form.Label>
                    <Form.Control ref={this.form.name} type="name" placeholder="Please enter your recipe's Name" />                
                </Form.Group>

                <Form.Group controlId="recipeIngredients">
                    <Form.Label>Ingredients</Form.Label>                    
                </Form.Group>

                <Form.Group controlId="recipeInstruction">
                    <Form.Label>Instruction</Form.Label>
                    <Form.Control lang="dv" className={"dvTextArea"} ref={this.form.instruction} as="textarea" placeholder="Your recipe" />
                </Form.Group>

                <Form.Group controlId="cookTime">
                    <Form.Label>Est. Prepare Time</Form.Label>                    
                </Form.Group>

                <Form.Group controlId="estPrice">
                    <Form.Label>Estimated Price</Form.Label>                    
                </Form.Group>

                <ImageUploader
                    withIcon={true}
                    withPreview={true}
                	buttonText='Choose images'
                	onChange={this.onDrop}
                	imgExtension={['.jpg', '.gif', '.png', '.gif']}
                	maxFileSize={5242880}
                />
            <Button variant="primary" type="button" onClick={this.onSubmit}>
                Submit
            </Button>

            </Form>
        );
    }
}


function RecipeDirectoryView(props){    
    return (
        props.data.length == 0?
        <div>No recipe found</div>
        :
        props.data.map(recipe => {
            return (<RecipeListItem 
                key={recipe._id} 
                mutable={true} 
                onRemove={() => props.onRemove(recipe)} 
                onEdit={()=> props.onEdit(recipe) }
                {...recipe} 
                />);
        })
    );
};

class ListMyRecipe extends React.Component{
    static contextType = AuthContext;
    state={
        data: []
    }
    onRemove = (recipe)=>{
        this.context.api.recipe.removeRecipe(recipe._id, 
            (result) =>{
                const newDataLst = this.state.data.filter(r => r._id !== recipe._id);
                this.setState({data: newDataLst});
            }
            );
    }
    onEdit = (recipe) => {
        console.log("Editing recipe", recipe);
    }

    loadRecipe(){
        this.context.api.recipe.listRecipe(response => this.setState({data: response.data.recipes}), true);
    }
    componentDidMount(){
        this.loadRecipe();
    }    
    submitCallback = (result) => {
        this.loadRecipe();
    }
    render(){
        return (
            <Tabs>
                <Tab eventKey="listRecipe" title="List Recipe">
            <RecipeDirectoryView
                onRemove={this.onRemove.bind(this)}
                onEdit={this.onEdit.bind(this)}
                data={this.state.data}
            />
            </Tab>
            <Tab eventKey="newRecipe" title="New Recipe">
            <CreateRecipe    
                submitCallback={this.submitCallback}
                />
            </Tab>
            </Tabs>
        )
    }
}

const MyRecipe = ()=> (
    <>
        <ListMyRecipe/>        
    </>
);

export {
    MyRecipe
};