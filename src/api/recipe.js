import{ Endpoint } from "./base";

export default class RecipeAPI extends Endpoint{
    removeRecipe(_id, success){
      const body = `
      mutation{
        removeRecipe(recipeId:"${_id}")
      }
      `;
      return this.base.sendRequest(body)
          .then( response => {
              success(response);
          });
    }

    fetchRecipe(success, id){
      const body = `query{
        recipes(id:"${id}"){
          _id
          name
          instruction
          pictures
          rating
          price
          creator{
            name
          }
        }
      }
    `;
    return this.base.sendRequest(body)
          .then( response => {
            if(response.data)
              success(response);
          });
    }

    listRecipe(success, own=false){
        const ownParam = own?"(owner:true)":"";

        const body = `
        query{
            recipes${ownParam}{
              _id
              name
              instruction
              pictures
              rating
              price
              creator{
                name
              }
            }
          }
        `;
        return this.base.sendRequest(body)
          .then( response => {
            if(response.data)
              success(response);
          });
    }

    createRecipe({name, instruction, pictures}, success){
        instruction = instruction.replace(/(?:\r\n|\r|\n)/g, '<br>');
        const pic_arr_str = JSON.stringify(pictures);
        const body = `
        mutation{
            createRecipe(recipeInput:{
              name: "${name}",
              instruction: "${instruction}",
              pictures: ${pic_arr_str}
            }){
              _id
            }
          }
        `;
        return this.base.sendRequest(body).then(
            (response) => {
                success(response);                
            }
        );
    }
}