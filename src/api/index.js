import AuthAPI from "./auth";
import RecipeAPI from "./recipe";
import BaseWebAPI from "./base";
import RestaurantAPI from "./restaurant";
import ChefAPI from "./chef";

class RootWebAPI extends BaseWebAPI{
    constructor(token){
        super(token);
        this.auth = new AuthAPI(this);
        this.recipe = new RecipeAPI(this);
        this.restaurant = new RestaurantAPI(this);
        this.chef = new ChefAPI(this);
    }
};

export default RootWebAPI;