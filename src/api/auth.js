import { Endpoint } from "./base";

export default class AuthAPI  extends Endpoint{   
    
    restaurantSignup(params){
        let {address, name, email, password, description, island, thumbnail} = params;
        const body = `
        mutation{
            createRestaurant(restaurantInput:{
            address: "${address}", 
            name: "${name}",
            email:"${email}",
            password:"${password}",
            description:"${description}",
            island:"${island}",
            thumbnail: "${thumbnail}"
            }){
            _id
            }
        }
        `;
        return this.base.sendRequest(body).then(
            (response) => {
                console.log(response);
                return response;
            }
        );
    }

    getUserData(success){
        const body = `
        query{
            user{
              type
              _id
              restaurant{
                  _id
              }
              chef{
                  _id
              }
            }
          }
        `;
        
        return this.base.sendRequest(body).then(
            (response) => {
                const user = response.data.user;
                if(success)
                    success(user);
                return user;
            }
        ).catch(
            ex => {
                console.log(ex);
            }
        );
    }

    fetchUserData(){
        const body = `
        query{
            user{
              type
            }
          }
        `;
        
        return this.base.sendRequest(body).then(
            (response) => {
                const user = response.data.user;
                return user.type;
            }
        ).catch(
            ex => {
                console.log(ex);
            }
        );
    }

    getUserName(){
        const body = `
        query{
            user{
              name
            }
          }
        `;
        
        return this.base.sendRequest(body).then(
            (response) => {
                const user = response.data.user;
                return user.name;
            }
        ).catch(
            ex => {
                console.log(ex);
            }
        );
    }

    login(args, success, error){
        const {email, password} = args;
        const body = `query{\n  login(email:"${email}", password:"${password}"){\n  user_id\n    token\n    token_expiration\n type\n }\n}`;
        this.base.sendRequest(body)
            .then(response => {
                //console.log("Response", response);   
                const errorMsg = this.base.fetchGraphqlError(response);
                error(errorMsg);
                if(!errorMsg){
                    success(response);
                }
            });

    }

    refreshToken(success, error){
        const body = `
        mutation{
            refreshToken{
              token
              user_id    
            }
          }
        `;
        return this.base.sendRequest(body)
          .then( response => {
            const errorMsg = this.base.fetchGraphqlError(response);                                
            if(!errorMsg){
                if(success)
                    success(response.data.refreshToken);
            }else{
                error(errorMsg);
            }
          });
    }

    getToken(token,success, error){
        const body = `
        query{
            getToken{
              token,
              remaining_time
            }
          }
        `;
        return this.base.sendRequest(body).then(
            (response) => {
                const errorMsg = this.base.fetchGraphqlError(response);                    
                if(!errorMsg){
                    if(success)
                        success(response.data.getToken);
                }else{
                    error(errorMsg);
                }
                return response;
            }
        );
    }
}
