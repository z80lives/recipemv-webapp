import{ Endpoint } from "./base";

export default class RestaurantAPI extends Endpoint{

    updateProfilePic(_id, profilePic, success){
      const body = `
      mutation{
        updateRestaurant(restaurantId:"${_id}", restaurantInput:{
          thumbnail: "${profilePic}"    
        }){
          _id
          thumbnail  
        }
      }
      `;
       
      return this.base.sendRequest(body).then(
        (response) => {
            const data = response.data.restaurant;
            if(success)
                success(data);
            return data;
        }
    );
    }

    getRestaurantData(_id, success){
      const body = `
      query{
        restaurant(restaurantId: "${_id}"){
          _id
          contact{
            address
            island
            phone_number
            website
            twitter
            facebook
            instagram
          }
          creator{
            name
            type
          }
          thumbnail
        }
      }
        `;
        
        return this.base.sendRequest(body).then(
            (response) => {
                const data = response.data.restaurant;
                if(success)
                    success(data);
                return data;
            }
        );
    }
    
    listRestaurants(success){
        const body = `
        query{
          restaurants{
            description
            creator{
              name
              email
            }
            contact{
              address
              island
            }
            thumbnail
          }
        }
        `;
        return this.base.sendRequest(body)
          .then( response => {
            if(response.data)
              success(response);
          });
    }
}
