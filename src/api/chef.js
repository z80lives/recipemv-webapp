import{ Endpoint } from "./base";

export default class RestaurantAPI extends Endpoint{
    chefSignup(params){
        let {address, name, email, password, description, island, thumbnail} = params;
        const body = `
        mutation{
            createChef(chefInput:{
                address: "${address}", 
                name: "${name}",
                email:"${email}",
                password:"${password}",
                description:"${description}",
                island:"${island}",
                thumbnail: "${thumbnail}"
            }){
            _id
            }
        }
        `;

        return this.base.sendRequest(body).then(
            (response) => {
                console.log(response);
                return response;
            }
        );
    }

    updateProfilePic(_id, profilePic, success){
      const body = `
      mutation{
        updateChef(chefId:"${_id}", chefInput:{
          thumbnail: "${profilePic}"    
        }){
          _id
          thumbnail  
        }
      }
      `;
       
      return this.base.sendRequest(body).then(
        (response) => {
            const data = response.data.chef;
            if(success)
                success(data);
            return data;
        }
    );
    }


    updateCoverPic(_id, coverPic, success){
      const body = `
      mutation{
        updateChef(chefId:"${_id}", chefInput:{
          coverImage: "${coverPic}"    
        }){
          _id
          coverImage
        }
      }
      `;
       
      return this.base.sendRequest(body).then(
        (response) => {
            const data = response.data.updateChef;
            if(success)
                success(data);
            return data;
        }
    );
    }



    getChefData(_id, success, isUserId=false){
	const paramIsUser = isUserId?", isUserID: true": "";
      const body = `
      query{
        chef(chefId: "${_id}" ${paramIsUser}){
          _id
          contact{
            address
            island
            phone_number
            website
            twitter
            facebook
            instagram
          }
          account{
            name
            email
          }
          thumbnail
          coverImage
        }
      }
        `;
        
        return this.base.sendRequest(body).then(
            (response) => {
                const data = response.data.chef;
                if(success)
                    success(data);
                return data;
            }
        );
    }


    listChef(success){
        const body = `
        query{
            chefs{
              _id,
              contact{
                address
                island
                phone_number
                website
                twitter
                facebook
                instagram
              }
              account{
                name      
              }
              thumbnail
            }
          }
        `;
        return this.base.sendRequest(body).then(
            (response) => {
                success(response);
                return response;
            }
        );
    }
}
