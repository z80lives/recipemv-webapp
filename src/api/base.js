//const api_endpoint = "http://localhost:4000/graphql";
const api_endpoint = "/api/";

export class Endpoint{
    constructor(base){
        this.base = base;
    }
}

export default class BaseWebAPI{
    constructor(token){
        this.api_endpoint = api_endpoint;
        this.params = {
            "method": "POST",
            "headers": {
                "content-type": "application/json"
            }
        }
        this.token = token || null;
        if(token){
            this.params.headers = {
                ...this.params.headers,
                'Authorization': 'Bearer '+token
            }
        }
    }

    sendRequest(body){
        let query = {query: body};
        return fetch(this.api_endpoint, {
            ...this.params,
            body: JSON.stringify(query)
        }).then(r=>r.json())
        /*.then(response => {
            if(response.errors)
                throw response.errors;
            return response;
        })*/
        .catch(err => console.log("error", err))
        ;
    }

    fetchGraphqlError = (response) => {
        console.log("Response", response);
        if(response && response.errors){
            return "Sorry! "+response.errors[0].message;
        }
        return null;
    }
    
}