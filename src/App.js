import React from 'react';
import {Redirect} from "react-router-dom";
import Layout from "./views/Layout";
import 'bootstrap/dist/css/bootstrap.min.css';
import localization_init from "./utils/i18n";
import {useTranslation} from "react-i18next";
import {IntlProvider}  from "react-intl";

import localeData from "./locales/en.json";
import Holder from "holderjs";
import {
  Home, 
  RegisterRestaurant, 
  RegisterChef,
  RecipeDirectory,
  RecipeView, 
  MyRecipe,
  Video,
  Bakery,
  Profile,
  Health,
  Chef
} from "./views/pages";
import API from "./api";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import AuthContext from "./context/auth-context";
import { RestaurantDirectory } from './views/pages/Restaurant';

localization_init();

const global_config = {
  layout: {
    direction: "lr"
  },
  language: "dv-utf"
};


const MainLayout = (props) => (
  <div className="App">
    <header className="App-header">        
      <Layout {...props.isLoggedIn}>
        {props.children}
      </Layout>
    </header>
  </div>
);

function BodyRoutes(props){
  const { t } = useTranslation();
  return (
       <Switch>
         <Route path="/(create/recipe|އާރެސިޕީ)">
          <MyRecipe t={t}/>
        </Route>
        <Route path="/(recipe|ރެސިޕީ)/:id/">
          <RecipeView t={t}/>
        </Route>
        <Route path="/(recipe|ރެސިޕީ)/">
          <RecipeDirectory t={t}/>
        </Route>
        <Route path="/restaurant/new/">
          <RegisterRestaurant t={t}/>
        </Route>
        <Route path="/chef/new/">
          <RegisterChef t={t} />
        </Route>
        <Route path="/(chef|ޝެފް)">
          <Chef t={t}/>
        </Route>
        <Route path="/(restaurant|ރެސްޓޯރަނޓު)">
          <RestaurantDirectory t={t} />
        </Route>
        <Route path="/(bakery|ބޭކަރީ)">
          <Bakery t={t}/>
        </Route>
        <Route path="/(health|ސިއްޙަތު)">
          <Health t={t}/>
        </Route>
        <Route path="/(video|ވީޑިޔޯ)">
          <Video t={t}/>
        </Route>
        <Route path="/(profile|ޕްރޯފައިލު)">
          <Profile t={t}/>
        </Route>
        <Route path="/">
          <Home t={t}/>
        </Route>
      </Switch>
     
  );
}


class App extends React.Component{

  constructor(props){
    super(props);
    const token = localStorage.getItem('token') || null;
    const user_id = localStorage.getItem('user_id') || null;
    const user_type = localStorage.getItem('user_type') || null;
    this.state = {
      token: token,
      user_id: user_id,
      user_type: user_type,
      expiration: null,
      api: new API(token),
      redirect: false
    }
    this.tokenExpire = null;    
  }

  refreshToken = () =>{
    this.state.api.auth.refreshToken((response)=>{
      console.log("refresh", response);
      localStorage.setItem('token', response.token);    
      localStorage.setItem('user_id', response.user_id);
      this.setState({token: response.token, user_id: response.user_id, api: new API(response.token)});
    });
  };

  pollToken = () => {
      
    if(!this.state.token)
      return;
      
    this.state.api.auth.getToken(this.state.token, (response) => {      
      const remaining = Number(response.remaining_time.split("h")[0]);
      console.log("Remaining", remaining);
      if(remaining < 15){
        this.refreshToken();
      }
      if(this.tokenExpire)
        clearTimeout(this.tokenExpire);
      this.tokenExpire = setTimeout(this.pollToken, 5000);

    }, (errorMsg) => {
      console.log("Error", errorMsg);
    });
  }

  componentDidMount(){  
    this.pollToken();
  }

  login = (token, params, expiration) => {
    const {user_id, user_type} = params;
    localStorage.setItem('token', token);    
    localStorage.setItem('user_id', user_id);
    localStorage.setItem("user_type", user_type);
    this.setState({token: token, user_id: user_id, user_type: user_type, api: new API(token)});        
  };

  logout = () => {
    console.log("Logging out");
    localStorage.removeItem('token');    
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_type');
    this.api = new API();
    this.setState({token: null, user_id: null, user_type:null, api: new API()});    
  }

  render(){
      return(
	      <IntlProvider locale="dv" messages={localeData}>  
	      <AuthContext.Provider value={{
          token: this.state.token, 
          user_id: this.state.user_id,
          user_type: this.state.user_type,
          login: this.login,
          logout: this.logout,
          api:this.state.api
        }}> 
	      <Router>
          <MainLayout config={global_config} isLoggedIn={this.state.token==null}>
            <BodyRoutes/>
          </MainLayout>
        </Router>
	      
	  </AuthContext.Provider>
	      </IntlProvider>  
    );
  }
}

export default App;
